theodorus: theodorus.lpi theodorus.lpr
	lazbuild $<
test1: theodorus
	./$< --imagewidth=640 --numtriangles=16 --scale=76 --backcolor=FFFACDFF --linecolor=FF8C00FF --fillcolor=FFFFE0FF
test2: theodorus
	./$< -w 620 -n 160 -s 24 -b FFFACDFF -l FF8C00FF -f FFDB58FF -r
clean:
	rm -f theodorus
	rm -f image.png
