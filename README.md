# Spiral of Theodorus

## Overview

Command line tool that makes a picture of a Theodorus spiral.

## Usage

```
theodorus --imagewidth=800 --numtriangles=23 --scale=80.2 --backcolor="000000FF" --linecolor="00009080" --fillcolor="00007080"
theodorus -w 800 -n 23 -s 79.8 -b "00000000" -l "00009080" -f "00007080"
```

## Example

![Image](image.png)

## Compilation

This is a Lazarus project, using [BGRABitmap](https://github.com/bgrabitmap/bgrabitmap) (package *bgrabitmappack4nogui.lpk*).
