
unit Vectors;

interface

uses
  BGRABitmapTypes;

function Rotate(const AVector: TPointF; const ATheta: double): TPointF;
function Perpendicular(const AVector: TPointF; const AClockWise: boolean = FALSE): TPointF;
function Normalize(const AVector: TPointF): TPointF;
function Add(const AVector1, AVector2: TPointF): TPointF;
function Scale(const AVector: TPointF; const AFactor: double): TPointF;
function Mirror(const AVector: TPointF): TPointF;

implementation

function Rotate(const AVector: TPointF; const ATheta: double): TPointF;
{
  x' = x cos θ − y sin θ
  y' = x sin θ + y cos θ
}
begin
  with AVector do
  begin
    result.x := x * Cos(ATheta) - y * Sin(ATheta);
    result.y := x * Sin(ATheta) + y * Cos(ATheta);
  end;
end;

function Perpendicular(const AVector: TPointF; const AClockWise: boolean): TPointF;
{
  x' = x cos θ − y sin θ
  y' = x sin θ + y cos θ
}
var
  LSign: integer;
begin
  LSign := 1 - 2 * Ord(AClockWise);
  with AVector do
  begin
    result.x := -y * LSign;
    result.y :=  x * LSign;
  end;
end;

function Normalize(const AVector: TPointF): TPointF;
var
  LLength: double;
begin
  with AVector do
  begin
    LLength := Sqrt(x * x + y * y);
    result.x := x / LLength;
    result.y := y / LLength;
  end;
end;

function Add(const AVector1, AVector2: TPointF): TPointF;
begin
  result.x := AVector1.x + AVector2.x;
  result.y := AVector1.y + AVector2.y;
end;

function Scale(const AVector: TPointF; const AFactor: double): TPointF;
begin
  with AVector do
  begin
    result.x := x * AFactor;
    result.y := y * AFactor;
  end;
end;

function Mirror(const AVector: TPointF): TPointF;
begin
  with AVector do
  begin
    result.x :=  x;
    result.y := -y;
  end;
end;

end.
