program theodorus;

(* Spiral of Theodorus *)

{$MODE OBJFPC}{$H+}

uses
{$IFDEF UNIX}
  cthreads,
{$ENDIF}
  Classes, SysUtils, CustApp, Math, BGRABitmap, BGRABitmapTypes, Vectors;

type
  TImageCreator = class(TCustomApplication)
  protected
    procedure DoRun; override;
  public
    constructor Create(TheOwner: TComponent); override;
    destructor Destroy; override;
    procedure WriteHelp; virtual;
  private
    procedure CreateTheodorusSpiral(const AImageWidth, AImageHeight, ANumTriangles: integer; const AScale: double; const ABack, ALine, AFill: string; const AReverse: boolean);
  end;

procedure TImageCreator.CreateTheodorusSpiral(const AImageWidth, AImageHeight, ANumTriangles: integer; const AScale: double; const ABack, ALine, AFill: string; const AReverse: boolean);

var
  LCenter, LPoint1, LPoint2, LPoint3: TPointF;

var
  LBitmap: TBGRABitmap;
  LLine, LFill, LBack: TBGRAPixel;

type
  TData = record
    FPoint2: TPointF;
    FPoint3: TPointF;
  end;

var
  LData: array of TData = nil;
  
const
  CPenWidth = 1;
  CFileName = 'image.png';

procedure DrawLine(APoint1, APoint2: TPointF);
begin
  APoint1 := Add(LCenter, Mirror(Scale(APoint1, AScale)));
  APoint2 := Add(LCenter, Mirror(Scale(APoint2, AScale)));

  LBitmap.DrawLineAntialias(APoint1.x, APoint1.y, APoint2.x, APoint2.y, LLine, CPenWidth);
end;

procedure DrawTriangle(APoint1, APoint2, APoint3: TPointF);
begin
  APoint1 := Add(LCenter, Mirror(Scale(APoint1, AScale)));
  APoint2 := Add(LCenter, Mirror(Scale(APoint2, AScale)));
  APoint3 := Add(LCenter, Mirror(Scale(APoint3, AScale)));

  LBitmap.DrawPolyLineAntialias([APoint1, APoint2, APoint3, APoint1], LLine, CPenWidth, LFill);
end;

var
  i: integer;

begin
  LCenter := PointF(Pred(AImageWidth) / 2, Pred(AImageHeight) / 2);

  LBack := StrToBGRA(ABack);
  LLine := StrToBGRA(ALine);
  LFill := StrToBGRA(AFill);

  LBitmap := TBGRABitmap.Create(AImageWidth, AImageHeight, LBack);

  LPoint1 := PointF(0.0, 0.0);
  LPoint2 := PointF(1.0, 0.0);

  if AReverse then
  begin
    SetLength(LData, ANumTriangles);
    for i := 0 to Pred(ANumTriangles) do
    begin
      LPoint3 := Add(LPoint2, Normalize(Perpendicular(LPoint2)));
      LData[i].FPoint2 := LPoint2;
      LData[i].FPoint3 := LPoint3;
      LPoint2 := LPoint3;
    end;
    for i := Pred(ANumTriangles) downto 0 do
    begin
      DrawTriangle(LPoint1, LData[i].FPoint2, LData[i].FPoint3);
    end;
    SetLength(LData, 0);
  end else
  begin
    for i := 1 to ANumTriangles do
    begin
      LPoint3 := Add(LPoint2, Normalize(Perpendicular(LPoint2)));
      DrawTriangle(LPoint1, LPoint2, LPoint3);
      LPoint2 := LPoint3;
    end;
  end;

  LBitmap.SaveToFile(CFileName);
  LBitmap.Free;
end;

procedure TImageCreator.DoRun;
const
  CImageWidth = 640;
  CNumTriangles = 15;
  CScale = 64.0;
  CBackColor = '101010FF';
  CLineColor = '009000FF';
  CFillColor = '006000FF';
var
  LError: string;
  LImageWidth, LNumTriangles: integer;
  LScale: double;
  LBackColor, LLineColor, LFillColor: string;
  LReverse: boolean;
begin
  LError := CheckOptions('hw:n:s:b:l:f:r', 'help imagewidth: numtriangles: scale: backcolor: linecolor: fillcolor: reverse');

  if LError <> '' then
  begin
    ShowException(Exception.Create(LError));
    Terminate;
    Exit;
  end;

  if HasOption('h', 'help') then
  begin
    WriteHelp;
    Terminate;
    Exit;
  end;

  if HasOption('w', 'imagewidth') then LImageWidth := StrToIntDef(GetOptionValue('w', 'imagewidth'), CImageWidth) else LImageWidth := CImageWidth;
  if HasOption('n', 'numtriangles') then LNumTriangles := StrToIntDef(GetOptionValue('n', 'numtriangles'), CNumTriangles) else LNumTriangles := CNumTriangles;
  if HasOption('s', 'scale') then LScale := StrToFloatDef(GetOptionValue('s', 'scale'), CScale) else LScale := CScale;
  if HasOption('b', 'backcolor') then LBackColor := GetOptionValue('b', 'backcolor') else LBackColor := CBackColor;
  if HasOption('l', 'linecolor') then LLineColor := GetOptionValue('l', 'linecolor') else LLineColor := CLineColor;
  if HasOption('f', 'fillcolor') then LFillColor := GetOptionValue('f', 'fillcolor') else LFillColor := CFillColor;
  
  LReverse := HasOption('r', 'reverse');
  
  CreateTheodorusSpiral(LImageWidth, LImageWidth, LNumTriangles, LScale, LBackColor, LLineColor, LFillColor, LReverse);

  Terminate;
end;

constructor TImageCreator.Create(TheOwner: TComponent);
begin
  inherited Create(TheOwner);
  StopOnException := True;
end;

destructor TImageCreator.Destroy;
begin
  inherited Destroy;
end;

procedure TImageCreator.WriteHelp;
begin
  writeln('Usage: ', ExeName, ' -h');
end;

var
  Application: TImageCreator;
  
begin
  Application := TImageCreator.Create(nil);
  Application.Title := 'Spiral of Theodorus';
  Application.Run;
  Application.Free;
end.
